--liquibase formatted sql

--changeset create_match_table:1
CREATE TABLE public.user_score
(
    id             int8 NOT NULL,
    final_position int4 NULL,
    score          int4 NULL,
    app_user_id    int8 NULL,
    match_id       int8 NULL,
    CONSTRAINT user_score_pkey PRIMARY KEY (id),
    CONSTRAINT match_fk FOREIGN KEY (match_id) REFERENCES match (id),
    CONSTRAINT user_fk FOREIGN KEY (app_user_id) REFERENCES app_user (id)
);

--changeset create_match_table:2
CREATE SEQUENCE IF NOT EXISTS public.user_score_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
