--liquibase formatted sql

--changeset create_game_table:1
CREATE TABLE IF NOT EXISTS public.game
(
    id              int8         NOT NULL,
    font_name       varchar(255) NOT NULL DEFAULT 'league-gothic',
    icon            varchar(255) NOT NULL DEFAULT 'game-console',
    "name"          varchar(255) NOT NULL,
    score_matters   bool         NOT NULL,
    victory_matters bool         NOT NULL,
    CONSTRAINT game_pkey PRIMARY KEY (id)
);

--changeset create_game_table:2
CREATE SEQUENCE IF NOT EXISTS public.game_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
