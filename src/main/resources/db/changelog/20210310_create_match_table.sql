--liquibase formatted sql

--changeset create_match_table:1
CREATE TABLE IF NOT EXISTS public."match"
(
    id      int8 NOT NULL,
    "date"  timestamp WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP,
    game_id int8 NOT NULL ,
    CONSTRAINT match_pkey PRIMARY KEY (id),
    CONSTRAINT game_fk FOREIGN KEY (game_id) REFERENCES game (id)
);

--changeset create_match_table:2
CREATE SEQUENCE IF NOT EXISTS public.match_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
