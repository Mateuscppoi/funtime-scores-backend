--liquibase formatted sql

--changeset create_user:1
CREATE TABLE IF NOT EXISTS public."app_user"
(
    id     int8         NOT NULL,
    "name" varchar(255) NOT NULL ,
    CONSTRAINT app_user_pkey PRIMARY KEY (id)
);

--changeset create_user:2
CREATE SEQUENCE IF NOT EXISTS public.user_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
