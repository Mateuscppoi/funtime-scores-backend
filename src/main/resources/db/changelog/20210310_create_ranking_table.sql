--liquibase formatted sql

--changeset create_ranking_table:1
CREATE TABLE IF NOT EXISTS public.ranking
(
    id             int8         NOT NULL,
    game_id        int8         NULL,
    matches_played int4         NULL,
    "name"         varchar(255) NULL,
    players_count  int4         NULL,
    "position"     int4         NULL,
    ranking_type   varchar(255) NULL,
    total_score    int4         NULL,
    user_id        int8         NULL,
    wins_count     int4         NULL,
    CONSTRAINT ranking_pkey PRIMARY KEY (id)
);

--changeset create_ranking_table:2
CREATE SEQUENCE IF NOT EXISTS public.ranking_seq
    INCREMENT BY 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
