package br.com.anymarket.funtimescoresbackend.dto;

import br.com.anymarket.funtimescoresbackend.entity.Ranking;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
@JsonIgnoreProperties
public class RankingDTO {

    private List<String> ignoredColumns;
    private List<Ranking> rankingList;
}
