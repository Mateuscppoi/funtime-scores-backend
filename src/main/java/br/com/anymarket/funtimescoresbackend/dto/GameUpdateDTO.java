package br.com.anymarket.funtimescoresbackend.dto;

import br.com.anymarket.funtimescoresbackend.entity.Match;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class GameUpdateDTO {

    private String name;

    private String icon;

    private String fontName;

    private boolean victoryMatters;

    private boolean scoreMatters;
}
