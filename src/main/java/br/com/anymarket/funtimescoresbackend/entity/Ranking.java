package br.com.anymarket.funtimescoresbackend.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Ranking {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "ranking_sequence")
    @SequenceGenerator(name = "ranking_sequence", sequenceName = "ranking_seq", allocationSize = 1)
    private Long id;

    private Integer position;

    private String name;

    @Enumerated(EnumType.STRING)
    private RankingType rankingType;

    private Long gameId;

    private Long userId;

    private Integer playersCount;

    private Integer matchesPlayed;

    private Integer winsCount;
    private Integer secondPlaceCount;
    private Integer thirdPlaceCount;

    private Integer totalScore;
}
