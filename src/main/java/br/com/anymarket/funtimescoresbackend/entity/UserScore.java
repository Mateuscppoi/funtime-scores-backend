package br.com.anymarket.funtimescoresbackend.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "user_score")
public class UserScore {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_score_sequence")
    @SequenceGenerator(name = "user_score_sequence", sequenceName = "user_score_seq", allocationSize = 1)
    private Long id;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "app_user_id")
    private User user;

    private Integer score;

    @Column(name = "final_position")
    private Integer finalPosition;

    public boolean isFirstPlace() {
        return finalPosition == 1;
    }

    public boolean isSecondPlace() {
        return finalPosition == 2;
    }

    public boolean isThirdPlace() {
        return finalPosition == 3;
    }
}
