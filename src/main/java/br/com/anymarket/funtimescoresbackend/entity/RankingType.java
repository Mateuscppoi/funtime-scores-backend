package br.com.anymarket.funtimescoresbackend.entity;

public enum RankingType {

    GAME, USER, GAME_MATCH
}
