package br.com.anymarket.funtimescoresbackend.util;

public abstract class AppConstants {

    public static final String DELETE_USER_MESSAGE = "Todos os jogadores merecem ser lembrados por seus feitos.";
    public static final String DELETE_GAME_MESSAGE = "Todos os jogos merecem seus lugares na lista (por pior que eles possam ser).";
    public static final String DELETE_MATCH_MESSAGE = "Uma partida sempre fica na memória de alguem, não apague a lembrança dos outros (isso é errado se vc não sabia)";
}
