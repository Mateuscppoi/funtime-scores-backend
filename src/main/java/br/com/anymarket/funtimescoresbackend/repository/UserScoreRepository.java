package br.com.anymarket.funtimescoresbackend.repository;

import br.com.anymarket.funtimescoresbackend.entity.User;
import br.com.anymarket.funtimescoresbackend.entity.UserScore;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserScoreRepository extends JpaRepository<UserScore, Long> {

    List<UserScore> findAllByUser(User user);

}
