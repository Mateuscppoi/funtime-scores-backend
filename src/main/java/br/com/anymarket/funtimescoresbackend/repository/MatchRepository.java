package br.com.anymarket.funtimescoresbackend.repository;

import br.com.anymarket.funtimescoresbackend.entity.Match;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface MatchRepository extends JpaRepository<Match, Long> {

}
