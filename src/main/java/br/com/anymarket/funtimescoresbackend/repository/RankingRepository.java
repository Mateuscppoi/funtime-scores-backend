package br.com.anymarket.funtimescoresbackend.repository;

import br.com.anymarket.funtimescoresbackend.entity.Ranking;
import br.com.anymarket.funtimescoresbackend.entity.RankingType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface RankingRepository extends JpaRepository<Ranking, Long> {

    List<Ranking> findAllByRankingType(RankingType rankingType);
    List<Ranking> findAllByRankingTypeAndGameId(RankingType rankingType, Long gameId);
    Optional<Ranking> findAllByRankingTypeAndUserId(RankingType rankingType, Long user);
}
