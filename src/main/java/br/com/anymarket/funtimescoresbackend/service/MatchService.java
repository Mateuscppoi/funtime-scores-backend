package br.com.anymarket.funtimescoresbackend.service;


import br.com.anymarket.funtimescoresbackend.dto.MatchDTO;
import br.com.anymarket.funtimescoresbackend.entity.Game;
import br.com.anymarket.funtimescoresbackend.entity.Match;
import br.com.anymarket.funtimescoresbackend.entity.User;
import br.com.anymarket.funtimescoresbackend.entity.UserScore;
import br.com.anymarket.funtimescoresbackend.repository.MatchRepository;
import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class MatchService {

    private final MatchRepository matchRepository;
    private final GameService gameService;
    private final RankingService rankingService;


    public MatchService(MatchRepository matchRepository, GameService gameService, RankingService rankingService) {
        this.matchRepository = matchRepository;
        this.gameService = gameService;
        this.rankingService = rankingService;
    }

    public List<Match> getAllMatches() {
        return matchRepository.findAll();
    }

    public void createMatch(Long gameId, Match match) {
        Preconditions.checkArgument(isNull(match.getId()), "Não permitido inserir o ID da partida por esse endpoint.");
        Preconditions.checkArgument(!match.getUserScores().isEmpty(), "Não é permitido criar uma partida sem scores dos jogadores");
        calculateMatchScores(match);
        gameService.insertOrUpdateMatch(gameId, match);
        rankingService.createOrUpdateRanks(gameId);
    }

    private void calculateMatchScores(Match match) {
        match.getUserScores().sort(Comparator.comparing(UserScore::getScore).reversed());
        AtomicInteger index = new AtomicInteger(0);
        match.getUserScores().forEach(score -> score.setFinalPosition(index.incrementAndGet()));
        System.out.println(match.getUserScores());
    }

    public void updateMatch(Long gameId, Match dto) {
        Preconditions.checkArgument(nonNull(dto.getId()), "É necessário inserir o id da partida para atualizar");
        Preconditions.checkArgument(dto.getUserScores().isEmpty(), "Não é permitido criar uma partida sem scores dos jogadores");
        gameService.insertOrUpdateMatch(gameId, dto);
        rankingService.createOrUpdateRanks(gameId);
    }

    @Transactional(readOnly = true)
    public List<MatchDTO> getAllMatchesFromGame(Long gameId) {
        Optional<Game> optionalGame = gameService.getGameById(gameId);
        return optionalGame
                .map(game -> game.getMatches().stream()
                        .map(this::mapToDTO)
                        .collect(Collectors.toList()))
                .orElseGet(Lists::newArrayList);
    }

    private MatchDTO mapToDTO(Match match) {
        MatchDTO dto = new MatchDTO();
        dto.setId(match.getId());
        dto.setDate(match.getDate());
        dto.setUserScores(match.getUserScores());
        dto.setPlayersCount(match.getUserScores().size());

        String firstPlace = match.getUserScores().stream()
                .filter(UserScore::isFirstPlace)
                .map(UserScore::getUser)
                .map(User::getName)
                .findFirst()
                .orElse("");

        String secondPlace = match.getUserScores().stream()
                .filter(UserScore::isSecondPlace)
                .map(UserScore::getUser)
                .map(User::getName)
                .findFirst()
                .orElse("");

        String thirdPlace = match.getUserScores().stream()
                .filter(UserScore::isThirdPlace)
                .map(UserScore::getUser)
                .map(User::getName)
                .findFirst()
                .orElse("-");

        dto.setFirstPlace(firstPlace);
        dto.setSecondPlace(secondPlace);
        dto.setThirdPlace(thirdPlace);

        Integer totalScore = match.getUserScores().stream().map(UserScore::getScore).reduce(0, Integer::sum);

        dto.setTotalScore(totalScore);

        return dto;
    }
}
