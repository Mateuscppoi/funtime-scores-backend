package br.com.anymarket.funtimescoresbackend.service;

import br.com.anymarket.funtimescoresbackend.entity.*;
import br.com.anymarket.funtimescoresbackend.repository.RankingRepository;
import lombok.extern.log4j.Log4j2;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Log4j2
@Service
public class RankingService {

    private final RankingRepository rankingRepository;
    private final GameService gameService;
    private final UserService userService;
    private final UserScoreService scoreService;


    public RankingService(RankingRepository rankingRepository, GameService gameService, UserService userService, UserScoreService scoreService) {
        this.rankingRepository = rankingRepository;
        this.gameService = gameService;
        this.userService = userService;
        this.scoreService = scoreService;
    }

    @Async
    public void createOrUpdateRanks(Long gameId) {
        Optional<Game> optionalGame = gameService.getGameById(gameId);
        optionalGame.ifPresent(game -> {
            log.info("Iniciando atualização de ranks");
            createOrUpdateGameRank(game);
            createOrUpdateMatchRank(game);
            log.info("Finalizada a atualização de ranks");
        });
    }

    @Async
    public void updateAllRanks() {
        List<Game> allGames = gameService.getAllGames();
        allGames.forEach(game -> {
            log.info("Iniciando atualização de ranks");
            createOrUpdateGameRank(game);
            createOrUpdateMatchRank(game);
            createOrUpdateUserRank();
            log.info("Finalizada a atualização de ranks");
        });
    }

    //todo: Implementar rank geral de usuário
    private void createOrUpdateUserRank() {
        List<User> users = userService.getAllUsers();
        List<Ranking> userRanking = new ArrayList<>();

        users.forEach(user -> {
            List<UserScore> userScores = scoreService.findAllUserScores(user);

            final Integer totalScore = userScores.stream().mapToInt(UserScore::getScore).sum();
            final Integer totalWins = (int) userScores.stream().filter(UserScore::isFirstPlace).count();
            final Integer totalSecondPlace = (int) userScores.stream().filter(UserScore::isSecondPlace).count();
            final Integer totalThirdPlace = (int) userScores.stream().filter(UserScore::isThirdPlace).count();

            Optional<Ranking> existingRanking = rankingRepository.findAllByRankingTypeAndUserId(RankingType.USER, user.getId());

            existingRanking.ifPresentOrElse(existing -> {
                existing.setMatchesPlayed(userScores.size());
                existing.setTotalScore(totalScore);
                existing.setWinsCount(totalWins);
                existing.setSecondPlaceCount(totalSecondPlace);
                existing.setThirdPlaceCount(totalThirdPlace);
                existing.setMatchesPlayed(userScores.size());
                userRanking.add(existing);
            }, () -> {
                Ranking ranking = new Ranking();
                ranking.setUserId(user.getId());
                ranking.setName(user.getName());
                ranking.setMatchesPlayed(userScores.size());
                ranking.setTotalScore(totalScore);
                ranking.setWinsCount(totalWins);
                ranking.setSecondPlaceCount(totalSecondPlace);
                ranking.setThirdPlaceCount(totalThirdPlace);
                ranking.setMatchesPlayed(userScores.size());
                ranking.setRankingType(RankingType.USER);
                userRanking.add(ranking);
            });

        });

        userRanking.sort(Comparator.comparing(Ranking::getTotalScore).thenComparing(Ranking::getWinsCount).reversed());

        AtomicInteger count = new AtomicInteger(0);
        userRanking.forEach(rank -> {
            rank.setPosition(count.incrementAndGet());
        });

        rankingRepository.saveAll(userRanking);
    }

    private void createOrUpdateGameRank(Game game) {
        List<Ranking> gameRanks = rankingRepository.findAllByRankingType(RankingType.GAME);

        gameRanks.stream()
                .filter(rank -> rank.getGameId().equals(game.getId()))
                .findFirst()
                .ifPresentOrElse(existingRank -> {
                    existingRank.setMatchesPlayed(game.getMatches().size());
                    existingRank.setPlayersCount(getPlayers(game).size());
                }, () -> {
                    Ranking ranking = createNewRank(game);
                    gameRanks.add(ranking);
                });

        gameRanks.sort(Comparator.comparing(Ranking::getMatchesPlayed).thenComparing(Ranking::getPlayersCount));

        AtomicInteger counter = new AtomicInteger(0);
        gameRanks.forEach(rank -> rank.setPosition(counter.incrementAndGet()));

        rankingRepository.saveAll(gameRanks);

    }

    private Ranking createNewRank(Game game) {
        Ranking ranking = new Ranking();
        ranking.setRankingType(RankingType.GAME);
        ranking.setGameId(game.getId());
        ranking.setName(game.getName());
        ranking.setMatchesPlayed(game.getMatches().size());
        ranking.setPlayersCount(getPlayers(game).size());
        return ranking;
    }

    private List<User> getPlayers(Game game) {
        return game.getMatches().stream()
                .map(Match::getUserScores)
                .flatMap(Collection::stream)
                .map(UserScore::getUser)
                .distinct().collect(Collectors.toList());
    }

    private void createOrUpdateMatchRank(Game game) {
        List<Ranking> matchRank = rankingRepository.findAllByRankingTypeAndGameId(RankingType.GAME_MATCH, game.getId());
        List<User> players = getPlayers(game);

        players.forEach(player -> {
                    List<UserScore> userScores = getGameScoresForUser(game, player);

                    final Integer totalScore = userScores.stream().mapToInt(UserScore::getScore).sum();
                    final Integer totalWins = (int) userScores.stream().filter(UserScore::isFirstPlace).count();
                    final Integer totalSecondPlace = (int) userScores.stream().filter(UserScore::isSecondPlace).count();
                    final Integer totalThirdPlace = (int) userScores.stream().filter(UserScore::isThirdPlace).count();

                    matchRank.stream().filter(rank -> rank.getUserId().equals(player.getId())).findFirst().ifPresentOrElse(existingRank -> {
                        existingRank.setTotalScore(totalScore);
                        existingRank.setWinsCount(totalWins);
                        existingRank.setSecondPlaceCount(totalSecondPlace);
                        existingRank.setThirdPlaceCount(totalThirdPlace);
                        existingRank.setMatchesPlayed(userScores.size());
                    }, () -> {
                        Ranking ranking = new Ranking();
                        ranking.setUserId(player.getId());
                        ranking.setName(player.getName());
                        ranking.setTotalScore(totalScore);
                        ranking.setWinsCount(totalWins);
                        ranking.setSecondPlaceCount(totalSecondPlace);
                        ranking.setThirdPlaceCount(totalThirdPlace);
                        ranking.setMatchesPlayed(userScores.size());
                        ranking.setGameId(game.getId());
                        ranking.setRankingType(RankingType.GAME_MATCH);
                        matchRank.add(ranking);
                    });
                }
        );

        if (game.isScoreMatters() && game.isVictoryMatters()) {
            matchRank.sort(Comparator.comparing(Ranking::getTotalScore).thenComparing(Ranking::getWinsCount).reversed());
        } else if (game.isVictoryMatters()) {
            matchRank.sort(Comparator.comparing(Ranking::getWinsCount).reversed());
        } else {
            matchRank.sort(Comparator.comparing(Ranking::getTotalScore).reversed());
        }

        AtomicInteger count = new AtomicInteger(0);
        matchRank.forEach(rank -> {
            rank.setPosition(count.incrementAndGet());
        });
        rankingRepository.saveAll(matchRank);

    }

    private List<UserScore> getGameScoresForUser(Game game, User player) {
        return game.getMatches()
                .stream()
                .map(Match::getUserScores)
                .flatMap(Collection::stream)
                .filter(score -> score.getUser().getId().equals(player.getId()))
                .collect(Collectors.toList());
    }

    public List<Ranking> getRankWithType(RankingType rankingType) {
        return rankingRepository.findAllByRankingType(rankingType);
    }

    public List<Ranking> getRankWithTypeAndGameId(RankingType rankingType, Long gameId) {
        return rankingRepository.findAllByRankingTypeAndGameId(rankingType, gameId);
    }
}
