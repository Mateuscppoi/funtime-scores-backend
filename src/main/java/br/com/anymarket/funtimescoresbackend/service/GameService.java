package br.com.anymarket.funtimescoresbackend.service;


import br.com.anymarket.funtimescoresbackend.dto.GameUpdateDTO;
import br.com.anymarket.funtimescoresbackend.entity.Game;
import br.com.anymarket.funtimescoresbackend.entity.Match;
import br.com.anymarket.funtimescoresbackend.exceptions.GameAlreadyExistsException;
import br.com.anymarket.funtimescoresbackend.exceptions.MatchNotFoundException;
import br.com.anymarket.funtimescoresbackend.exceptions.UserAlreadyExistsException;
import br.com.anymarket.funtimescoresbackend.repository.GameRepository;
import com.google.common.base.Preconditions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Objects.isNull;

@Service
public class GameService {

    private final GameRepository gameRepository;

    public GameService(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    public List<Game> getAllGames() {
        return gameRepository.findAll();
    }

    public void createNewGame(Game game) {
        Preconditions.checkArgument(!isNullOrEmpty(game.getName()), "O nome do usuário é obrigatório.");
        Preconditions.checkArgument(isNull(game.getId()), "Não permitido inserir o ID do usuário por esse endpoint.");
        Preconditions.checkArgument(game.getMatches().isEmpty(), "Não é permitido criar um jogo com partidas registradas.");
        Optional<Game> existingGame = gameRepository.findByName(game.getName());
        if (existingGame.isPresent()) {
            throw new UserAlreadyExistsException();
        }
        gameRepository.save(game);
    }

    public void updateGame(Long id, GameUpdateDTO dto) {
//        Preconditions.checkArgument(!isNullOrEmpty(dto.getName()), "O nome do jogo é obrigatório.");
//        Optional<Game> optionalGame = gameRepository.findByName(dto.getName());
//        if (optionalGame.isPresent()) {
//            throw new GameAlreadyExistsException();
//        }
        Optional<Game> existingGame = gameRepository.findById(id);
        existingGame.ifPresent(game -> {
            game.setName(dto.getName());
            game.setIcon(dto.getIcon());
            game.setFontName(dto.getFontName());
            game.setVictoryMatters(dto.isVictoryMatters());
            game.setScoreMatters(dto.isScoreMatters());
            gameRepository.save(game);
        });
    }

    @Transactional
    public void insertOrUpdateMatch(Long gameId, Match match) {
        Optional<Game> existingGame = gameRepository.findById(gameId);
        existingGame.ifPresent(game -> {
            if (Objects.isNull(match.getId())) {
                game.getMatches().add(match);
            } else {
                game.getMatches().stream()
                        .filter(gameMatch -> gameMatch.getId().equals(match.getId()))
                        .findFirst()
                        .ifPresentOrElse(gameMatch -> {
                            gameMatch.setDate(match.getDate());
                            gameMatch.setUserScores(match.getUserScores());
                        }, MatchNotFoundException::new);
            }
            gameRepository.save(game);
        });
    }

    @Transactional(readOnly = true)
    public Optional<Game> getGameById(Long gameId) {
        return gameRepository.findById(gameId);
    }
}
