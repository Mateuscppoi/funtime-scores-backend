package br.com.anymarket.funtimescoresbackend.service;


import br.com.anymarket.funtimescoresbackend.dto.UserUpdateDTO;
import br.com.anymarket.funtimescoresbackend.entity.User;
import br.com.anymarket.funtimescoresbackend.exceptions.UserAlreadyExistsException;
import br.com.anymarket.funtimescoresbackend.repository.UserRepository;
import com.google.common.base.Preconditions;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.google.common.base.Strings.isNullOrEmpty;
import static java.util.Objects.isNull;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public void createNewUser(User user) {
        Preconditions.checkArgument(!isNullOrEmpty(user.getName()), "O nome do usuário é obrigatório.");
        Preconditions.checkArgument(isNull(user.getId()), "Não permitido inserir o ID do usuário por esse endpoint.");
        Optional<User> existingUser = userRepository.findByName(user.getName());
        if (existingUser.isPresent()) {
            throw new UserAlreadyExistsException();
        }
        userRepository.save(user);
    }

    public void updateUser(Long id, UserUpdateDTO dto) {
        Preconditions.checkArgument(!isNullOrEmpty(dto.getName()), "O nome do usuário é obrigatório.");
        Optional<User> optionalUser = userRepository.findByName(dto.getName());
        if (optionalUser.isPresent()) {
            throw new UserAlreadyExistsException();
        }
        Optional<User> existingUser = userRepository.findById(id);
        existingUser.ifPresent(user -> {
            user.setName(dto.getName());
            userRepository.save(user);
        });
    }
}
