package br.com.anymarket.funtimescoresbackend.service;

import br.com.anymarket.funtimescoresbackend.entity.User;
import br.com.anymarket.funtimescoresbackend.entity.UserScore;
import br.com.anymarket.funtimescoresbackend.repository.UserScoreRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserScoreService {

    private final UserScoreRepository userScoreRepository;

    public UserScoreService(UserScoreRepository userScoreRepository) {
        this.userScoreRepository = userScoreRepository;
    }

    public List<UserScore> findAllUserScores(User user) {
        return userScoreRepository.findAllByUser(user);
    }
}
