package br.com.anymarket.funtimescoresbackend.exceptions;

import org.springframework.http.HttpStatus;

public class MatchNotFoundException extends ApplicationException {

    private static final String MESSAGE = "Não foi encontrada a partida para atualizar.";

    public MatchNotFoundException() {
        super(MESSAGE, HttpStatus.NOT_FOUND);
    }
}
