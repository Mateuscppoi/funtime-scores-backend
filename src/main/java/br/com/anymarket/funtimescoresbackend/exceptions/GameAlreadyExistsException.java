package br.com.anymarket.funtimescoresbackend.exceptions;

import org.springframework.http.HttpStatus;

public class GameAlreadyExistsException extends ApplicationException {

    private static final String MESSAGE = "Já existe um jogo com o mesmo nome cadastrado";

    public GameAlreadyExistsException() {
        super(MESSAGE, HttpStatus.CONFLICT);
    }
}
