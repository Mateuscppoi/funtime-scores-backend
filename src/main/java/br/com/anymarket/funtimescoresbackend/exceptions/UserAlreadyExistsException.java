package br.com.anymarket.funtimescoresbackend.exceptions;

import org.springframework.http.HttpStatus;

public class UserAlreadyExistsException extends ApplicationException {

    private static final String MESSAGE = "Já existe um usuário com o mesmo nome cadastrado";

    public UserAlreadyExistsException() {
        super(MESSAGE, HttpStatus.CONFLICT);
    }
}
