package br.com.anymarket.funtimescoresbackend.api;


import br.com.anymarket.funtimescoresbackend.dto.ErrorResponse;
import br.com.anymarket.funtimescoresbackend.dto.MatchDTO;
import br.com.anymarket.funtimescoresbackend.entity.Match;
import br.com.anymarket.funtimescoresbackend.exceptions.ApplicationException;
import br.com.anymarket.funtimescoresbackend.service.MatchService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static br.com.anymarket.funtimescoresbackend.util.AppConstants.DELETE_MATCH_MESSAGE;


@RestController
@RequestMapping("/api/games/matches")
public class MatchApi {

    private final MatchService matchService;

    public MatchApi(MatchService matchService) {
        this.matchService = matchService;
    }

    @GetMapping()
    public ResponseEntity<?> getAllMatches() {
        List<Match> matchList = matchService.getAllMatches();
        return new ResponseEntity<>(matchList, HttpStatus.OK);
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<?> getAllMatchesFromGame(@PathVariable("gameId") Long gameId) {
        List<MatchDTO> matchList = matchService.getAllMatchesFromGame(gameId);
        return new ResponseEntity<>(matchList, HttpStatus.OK);
    }

    @PostMapping("/{gameId}")
    public ResponseEntity<?> createNewMatch(@PathVariable("gameId") Long gameId, @RequestBody Match match) {
        try {
            matchService.createMatch(gameId, match);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (ApplicationException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), e.getHttpStatus());
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{gameId}/{matchId}")
    public ResponseEntity<?> updateMatch(@PathVariable("gameId") Long gameId, @PathVariable("matchId") Long matchId, @RequestBody Match dto) {
        try {
            matchService.updateMatch(gameId, dto);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (ApplicationException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), e.getHttpStatus());
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteMatch(@PathVariable("id") Long id) {
        return new ResponseEntity<>(new ErrorResponse(DELETE_MATCH_MESSAGE), HttpStatus.METHOD_NOT_ALLOWED);
    }
}
