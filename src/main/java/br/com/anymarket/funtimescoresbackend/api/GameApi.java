package br.com.anymarket.funtimescoresbackend.api;

import br.com.anymarket.funtimescoresbackend.dto.ErrorResponse;
import br.com.anymarket.funtimescoresbackend.dto.GameUpdateDTO;
import br.com.anymarket.funtimescoresbackend.entity.Game;
import br.com.anymarket.funtimescoresbackend.exceptions.ApplicationException;
import br.com.anymarket.funtimescoresbackend.service.GameService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static br.com.anymarket.funtimescoresbackend.util.AppConstants.DELETE_GAME_MESSAGE;


@RestController
@RequestMapping("/api/games")
public class GameApi {

    private final GameService gameService;

    public GameApi(GameService gameService) {
        this.gameService = gameService;
    }

    @GetMapping
    public ResponseEntity<?> getAllGames() {
        List<Game> games = gameService.getAllGames();
        return new ResponseEntity<>(games, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createNewGame(@RequestBody Game game) {
        try {
            gameService.createNewGame(game);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (ApplicationException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), e.getHttpStatus());
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateGame(@PathVariable("id") Long id, @RequestBody GameUpdateDTO dto) {
        try {
            gameService.updateGame(id, dto);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (ApplicationException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), e.getHttpStatus());
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteGame(@PathVariable("id") Long id) {
        return new ResponseEntity<>(new ErrorResponse(DELETE_GAME_MESSAGE), HttpStatus.METHOD_NOT_ALLOWED);
    }
}
