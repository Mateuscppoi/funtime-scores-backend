package br.com.anymarket.funtimescoresbackend.api;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/api/test")
public class TestApi {

    private static final String SLASH = "/";

    @GetMapping("/time")
    public Test getTime() {
        return new Test();
    }

    public static void main(String[] args) {
        List<Long> list = Lists.newArrayList(1L);

        StringBuilder idAccountsParameter = new StringBuilder();
        for (int index = 0; index < list.size(); index++) {
            idAccountsParameter.append(list.get(index).toString());
            if (list.size() - 1  != index) {
                idAccountsParameter.append(",");
            }
        }

        System.out.println(idAccountsParameter);
    }

}
