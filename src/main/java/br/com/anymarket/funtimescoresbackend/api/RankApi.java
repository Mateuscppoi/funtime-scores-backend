package br.com.anymarket.funtimescoresbackend.api;

import br.com.anymarket.funtimescoresbackend.dto.RankingDTO;
import br.com.anymarket.funtimescoresbackend.entity.Ranking;
import br.com.anymarket.funtimescoresbackend.entity.RankingType;
import br.com.anymarket.funtimescoresbackend.service.RankingService;
import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/rank")
public class RankApi {

    private static final List<String> IGNORED_COLUMNS = Lists.newArrayList("rankingType", "id", "gameId", "userId");

    private final RankingService rankingService;

    public RankApi(RankingService rankingService) {
        this.rankingService = rankingService;
    }

    @GetMapping
    public ResponseEntity<?> getRank(@RequestParam("type") RankingType rankingType) {
        List<Ranking> rankingList = rankingService.getRankWithType(rankingType);
        return new ResponseEntity<>(new RankingDTO(IGNORED_COLUMNS, rankingList), HttpStatus.OK);
    }

    @GetMapping("/{gameId}")
    public ResponseEntity<?> getRank(@PathVariable("gameId") Long gameId, @RequestParam("type") RankingType rankingType) {
        List<Ranking> rankingList = rankingService.getRankWithTypeAndGameId(rankingType, gameId);
        return new ResponseEntity<>(new RankingDTO(IGNORED_COLUMNS, rankingList), HttpStatus.OK);
    }

    @PostMapping("/forceUpdate")
    public void forceUpdateRank() {
        rankingService.updateAllRanks();
    }
}
