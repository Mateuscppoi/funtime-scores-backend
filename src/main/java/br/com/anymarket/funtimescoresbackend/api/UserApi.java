package br.com.anymarket.funtimescoresbackend.api;

import br.com.anymarket.funtimescoresbackend.dto.ErrorResponse;
import br.com.anymarket.funtimescoresbackend.dto.UserUpdateDTO;
import br.com.anymarket.funtimescoresbackend.entity.User;
import br.com.anymarket.funtimescoresbackend.exceptions.ApplicationException;
import br.com.anymarket.funtimescoresbackend.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static br.com.anymarket.funtimescoresbackend.util.AppConstants.DELETE_USER_MESSAGE;


@RestController
@RequestMapping("/api/user")
public class UserApi {

    private final UserService userService;

    public UserApi(UserService userService) {
        this.userService = userService;
    }

    @GetMapping
    public ResponseEntity<?> getAllUser() {
        List<User> users = userService.getAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> createNewUser(@RequestBody User user) {
        try {
            userService.createNewUser(user);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (ApplicationException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), e.getHttpStatus());
        }
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @RequestBody UserUpdateDTO dto) {
        try {
            userService.updateUser(id, dto);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
        } catch (ApplicationException e) {
            return new ResponseEntity<>(new ErrorResponse(e.getMessage()), e.getHttpStatus());
        }
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        return new ResponseEntity<>(new ErrorResponse(DELETE_USER_MESSAGE), HttpStatus.METHOD_NOT_ALLOWED);
    }
}
